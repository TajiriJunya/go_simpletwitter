package handler

import (
	"go_simpleTwitter/database"
	"net/http"
	"regexp"

	"github.com/labstack/echo"
)

//ログイン画面の画面表示処理
func GetLogin() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Render(http.StatusOK, "login", nil)
	}
}

//ログイン処理
func Login() echo.HandlerFunc {
	return func(c echo.Context) error {
		//フォームに入力された文字を取得
		Account := c.FormValue("account")
		Password := c.FormValue("password")
		whitespase := regexp.MustCompile(`\s+`)
		//入力された文字列のバリデーションチェック
		if whitespase.MatchString(Account) || Account == "" {
			errormessage := map[string]string{"errorMessage": "アカウント名を入力してください"}
			return c.Render(http.StatusOK, "login", errormessage)
		}
		//入力されたユーザ情報を渡すDB処理に渡す
		loginUser := database.SelectLogin(Account, Password)
		if loginUser.ID == 0 {
			errormessage := map[string]string{"errorMessage": "アカウント名またはパスワードが間違っています"}
			return c.Render(http.StatusOK, "login", errormessage)
		}
		//入力されたアカウント名とパスワードにヒットしたユーザでログイン処理を行う
		if !sessionLogin(c, loginUser.ID) {
			return c.NoContent(http.StatusInternalServerError)
		}
		return c.Redirect(http.StatusFound, "/top")

	}
}
