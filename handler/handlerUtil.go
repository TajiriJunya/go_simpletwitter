package handler

//よく使用する処理をまとめている
import (
	"go_simpleTwitter/container"
	"go_simpleTwitter/database"
	"log"
	"strconv"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

//ログイン中のユーザの情報を取得
func getLoginUser(c echo.Context) container.User {
	sess, _ := session.Get("session", c)
	loginUserID, _ := sess.Values["loginUser"].(int)
	loginUser := database.SelectLoginUser(strconv.Itoa(loginUserID))

	return loginUser
}

//ログイン中なのかを判定
func loginCheck(c echo.Context) bool {
	sess, _ := session.Get("session", c)
	login, _ := sess.Values["login"].(bool)
	return login
}

//ログイン処理（セッションにログインするユーザ情報を記録）
func sessionLogin(c echo.Context, ID int) bool {

	sess, _ := session.Get("session", c)
	sess.Options = &sessions.Options{
		//有効な時間
		MaxAge: 86400 * 7,
		//trueでjsからのアクセス拒否
		HttpOnly: true,
	}
	//ログインするユーザのID
	sess.Values["loginUser"] = ID
	sess.Values["login"] = true
	if err := sess.Save(c.Request(), c.Response()); err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

//ログアウト処理（セッションのログインユーザの情報を0に）
func sessionLogout(c echo.Context) bool {

	sess, _ := session.Get("session", c)
	//ログアウト
	sess.Values["loginUser"] = 0
	sess.Values["login"] = false
	//状態を保存
	if err := sess.Save(c.Request(), c.Response()); err != nil {
		log.Fatal(err)
		return false
	}

	return true
}

//現在までの投稿で一番値が大きいIDの値を返す
func messageIdMax(tweet []container.UserMessage) int {
	maxNum := 0
	for _, message := range tweet {
		if maxNum <= message.ID {
			maxNum = message.ID
		}
	}
	return maxNum
}
