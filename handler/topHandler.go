package handler

import (
	"go_simpleTwitter/database"
	"log"
	"net/http"
	"regexp"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

//top画面表示処理
func GetTop() echo.HandlerFunc {
	return func(c echo.Context) error {
		//HTMLで表示しやすいようにマップに格納していく
		topContents := make(map[string]interface{})
		//つぶやきの内容を全件取得
		tweet := database.Gettop(c)
		topContents["tweetContents"] = tweet
		topContents["tweetNum"] = messageIdMax(tweet)
		sess, _ := session.Get("session", c)
		errorMessage, _ := sess.Values["errorMessage"].(string)
		topContents["errorMessage"] = errorMessage
		//セッションの情報を基にログインしているか否かで表示内容を分岐
		if loginCheck(c) {
			//ログインしていればログイン中のユーザ情報を取得し、表示内容に追加
			loginUser := getLoginUser(c)
			topContents["loginUser"] = loginUser
			return c.Render(http.StatusOK, "tweet", topContents)
		}
		return c.Render(http.StatusOK, "top", topContents)

	}
}

//新規投稿処理
func NewPost() echo.HandlerFunc {
	return func(c echo.Context) error {
		//フォームに入力された文字列を取得
		text := c.FormValue("tubuyaki")
		//ログイン中のユーザ情報を取得
		loginUser := getLoginUser(c)
		//入力された文字列に対してのバリデーション処理
		errorMessage := twieetformValid(text)
		if len(errorMessage) > 0 {
			//エラーメッセージが一文でもあば表示
			reContents := make(map[string]interface{})
			tweet := database.Gettop(c)
			reContents["loginUser"] = loginUser
			reContents["tweetContents"] = tweet
			reContents["errorMessages"] = errorMessage
			reContents["tweetNum"] = messageIdMax(tweet)
			return c.Render(http.StatusOK, "tweet", reContents)
		}
		if !database.InsertMessage(loginUser.ID, text) {
			log.Fatal("投稿失敗")
			return c.NoContent(http.StatusInternalServerError)
		}
		//新規投稿処理が正常に行われたら画面を再読み込みする
		log.Println("新規投稿1件")
		return c.Redirect(http.StatusFound, "/top")
	}
}

//入力された文字列に対してのバリデーション処理
func twieetformValid(text string) []string {

	errorMessages := []string{}
	whitespase := regexp.MustCompile(`\s+`)

	if whitespase.MatchString(text) || text == "" {
		errorMessages = append(errorMessages, "ツイートを入力くしてください")
	}

	if len([]rune(text)) > 140 {
		errorMessages = append(errorMessages, "ツイートは140文字以内の投稿にしてください")
	}

	return errorMessages
}
