package handler

import (
	"go_simpleTwitter/container"
	"go_simpleTwitter/database"
	"log"
	"net/http"
	"regexp"

	"github.com/labstack/echo"
)

//新規ユーザ登録画面表示処理（HTMLファイルを指定するのみ）
func GetSignin() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Render(http.StatusOK, "signin", nil)
	}
}

//新規ユーザ登録処理
func Signin() echo.HandlerFunc {
	return func(c echo.Context) error {
		//入力されたユーザ情報を構造体に格納していく
		var user container.User
		user.Account = c.FormValue("account")
		user.Name = c.FormValue("name")
		user.Password = c.FormValue("password")
		rePassword := c.FormValue("repassword")
		//入力されたユーザ情報に対してのバリデーション
		errorMessages := signinformValid(user, rePassword)
		//エラーメッセージが1文でもあればエラーメッセージ付きの新規登録画面を再表示させる
		if len(errorMessages) > 0 {
			return c.Render(http.StatusOK, "signin", errorMessages)
		}
		//入力されたユーザ情報を渡すDB処理に渡す
		if !database.InsertUser(user) {
			return c.NoContent(http.StatusOK)
		}
		//新規登録したユーザでログイン処理を行う
		loginUser := database.SelectLogin(user.Account, user.Password)
		if !sessionLogin(c, loginUser.ID) {
			return c.NoContent(http.StatusInternalServerError)
		}
		log.Println("新規ユーザ-1")
		//新規登録されたユーザでログインをした状態でtop画面へ遷移
		return c.Redirect(http.StatusFound, "/top")
	}
}

//入力されたユーザ情報に対してのバリデーション
func signinformValid(user container.User, rePassword string) []string {

	errorMessages := []string{}
	whitespase := regexp.MustCompile(`\s+`)
	accountFormatCode, _ := regexp.Compile("^([a-zA-Z0-9]{6,12})$")

	if whitespase.MatchString(user.Account) || user.Account == "" {
		errorMessages = append(errorMessages, "アカウント名を入力してください")
	} else if !accountFormatCode.MatchString(user.Account) {
		errorMessages = append(errorMessages, "アカウント名は半角英数字で6字以上12字以内で設定してください")
	} else if database.CheckAccount(user.Account) == true {
		errorMessages = append(errorMessages, "そのアカウント名は使用できません")
	}

	if whitespase.MatchString(user.Name) || user.Name == "" {
		errorMessages = append(errorMessages, "ユーザ名を入力してください")
	} else if len([]rune(user.Name)) > 20 {
		errorMessages = append(errorMessages, "ユーザ名は1字以上20字以内で設定してください")
	}

	if whitespase.MatchString(user.Password) || user.Password == "" {
		errorMessages = append(errorMessages, "パスワードを入力してください")
	} else if whitespase.MatchString(rePassword) || rePassword == "" {
		errorMessages = append(errorMessages, "確認用パスワードを入力してください")
	} else if len([]rune(user.Password)) > 12 || len([]rune(user.Password)) < 4 {
		errorMessages = append(errorMessages, "パスワードは4文字以上12文字以内で設定してください")
	} else if user.Password != rePassword {
		errorMessages = append(errorMessages, "パスワードが確認用パスワードと一致しません")
	}

	return errorMessages
}
