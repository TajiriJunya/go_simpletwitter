package handler

import (
	"fmt"
	"go_simpleTwitter/container"
	"go_simpleTwitter/database"
	"net/http"
	"regexp"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

//ユーザ編集画面表示処理
func GetSetting() echo.HandlerFunc {
	return func(c echo.Context) error {
		//ログインしていない状態で編集画面に遷移しようとした場合はエラーメッセージとともにtopに戻す
		if !loginCheck(c) {
			sess, _ := session.Get("session", c)
			sess.Values["errorMessage"] = "ログインされていません"
			return c.Redirect(http.StatusFound, "/top")
		}
		//現在ログイン中のユーザ情報のうち、アカウント名とユーザ名を画面表示した際に表示する
		loginUser := getLoginUser(c)
		loginUserBefor := make(map[string]interface{})
		loginUserBefor["Account"] = loginUser.Account
		loginUserBefor["Name"] = loginUser.Name
		return c.Render(http.StatusOK, "setting", loginUserBefor)
	}
}

//ユーザ情報編集処理
func UserEdit() echo.HandlerFunc {
	return func(c echo.Context) error {
		//入力されたユーザ情報を構造体に格納していく
		loginUser := getLoginUser(c)
		var user container.User
		user.Account = c.FormValue("account")
		user.Name = c.FormValue("name")
		user.Password = c.FormValue("password")
		rePassword := c.FormValue("repassword")
		//入力された文字列のバリデーションチェック
		errorMessages := settingformValid(user, rePassword, loginUser)
		if len(errorMessages) > 0 {
			//エラーメッセージがある場合は編集画面を再表示
			loginUserBefor := make(map[string]interface{})
			loginUserBefor["errorMessages"] = errorMessages
			loginUserBefor["Account"] = loginUser.Account
			loginUserBefor["Name"] = loginUser.Name
			return c.Render(http.StatusOK, "setting", loginUserBefor)
		}
		//入力されたユーザ情報を渡すDB処理に渡す
		if !database.UpdataUser(user, loginUser.ID) {
			fmt.Println("更新失敗")
			return c.NoContent(http.StatusOK)
		}
		//正常にユーザ情報の更新が行われた場合はtop画面へ遷移
		return c.Redirect(http.StatusFound, "/top")
	}
}

//入力された文字列のバリデーションチェック
func settingformValid(user container.User, rePassword string, loginUser container.User) []string {

	errorMessages := []string{}
	whitespase := regexp.MustCompile(`\s+`)
	accountFormatCode, _ := regexp.Compile("^([a-zA-Z0-9]{6,12})$")

	if whitespase.MatchString(user.Account) || user.Account == "" {
		errorMessages = append(errorMessages, "アカウント名を入力してください")
	} else if !accountFormatCode.MatchString(user.Account) {
		errorMessages = append(errorMessages, "アカウント名は半角英数字で6字以上12字以内で設定してください")
	} else if database.CheckAccount(user.Account) == true && loginUser.Account != user.Account {
		errorMessages = append(errorMessages, "このアカウント名は使用できません")
	}

	if whitespase.MatchString(user.Name) || user.Name == "" {
		errorMessages = append(errorMessages, "ユーザ名を入力してください")
	} else if len([]rune(user.Name)) > 20 {
		errorMessages = append(errorMessages, "ユーザ名は1字以上20字以内で設定してください")
	}

	if len(user.Password) != 0 && (len([]rune(user.Password)) > 12 || len([]rune(user.Password)) < 4) {
		errorMessages = append(errorMessages, "パスワードは4文字以上12文字以内で設定してください")
	} else if user.Password != rePassword {
		errorMessages = append(errorMessages, "パスワードが確認用パスワードと一致しません")
	}

	return errorMessages
}
