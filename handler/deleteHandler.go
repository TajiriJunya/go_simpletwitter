package handler

import (
	"go_simpleTwitter/database"
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

//投稿削除処理
func DeleteMessage() echo.HandlerFunc {
	return func(c echo.Context) error {
		//削除対象のつぶやきのIDを取得
		messageID, _ := strconv.ParseInt(c.FormValue("messageID"), 10, 0)
		//DB処理に渡す
		if !database.DeleteMessage(int(messageID)) {
			log.Fatal("削除失敗")
			return c.NoContent(http.StatusInternalServerError)
		}
		//正常に処理ができたら画面を再読み込み
		return c.Redirect(http.StatusFound, "/top")
	}
}

//ユーザアカウント削除処理
func DeleteUser() echo.HandlerFunc {
	return func(c echo.Context) error {
		getloginUser := getLoginUser(c)
		Password := c.FormValue("password")
		//ログイン中のアカウント情報とパスワードが一致するかを検証
		loginUser := database.SelectLogin(getloginUser.Account, Password)
		//一致しなければ変数に0が代入され、エラーメッセージとともに画面をさ表示
		if loginUser.ID == 0 {
			var errorMessages []string
			errorMessages = append(errorMessages, "パスワードが間違っています")
			loginUserBefor := make(map[string]interface{})
			loginUserBefor["errorMessages"] = errorMessages
			loginUserBefor["Account"] = getloginUser.Account
			loginUserBefor["Name"] = getloginUser.Name
			return c.Render(http.StatusOK, "setting", loginUserBefor)
		}
		//現在ログイン中のアカウントを削除
		if !database.DeleteUser(loginUser.ID) {
			log.Fatal("削除失敗")
			return c.NoContent(http.StatusInternalServerError)
		}
		//ログアウト処理
		if !sessionLogout(c) {
			c.NoContent(http.StatusInternalServerError)
		}
		//top画面へ遷移
		return c.Redirect(http.StatusFound, "/top")
	}
}
