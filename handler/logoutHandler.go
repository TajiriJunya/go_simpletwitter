package handler

import (
	"net/http"

	"github.com/labstack/echo"
)

//ログアウト処理
func Logout() echo.HandlerFunc {
	return func(c echo.Context) error {

		if !sessionLogout(c) {
			c.NoContent(http.StatusInternalServerError)
		}
		return c.Redirect(http.StatusFound, "/top")
	}
}
