package handler

import (
	"go_simpleTwitter/database"
	"log"
	"strconv"

	"github.com/labstack/echo"
)

//いいねの増減処理
func LikeUpdate() echo.HandlerFunc {
	return func(c echo.Context) error {
		//いいねされた箇所のつぶやきのIDといいねされての状態を取得
		messageNum := c.FormValue("messageID")
		likejudge := c.FormValue("likejudge")
		log.Println("ID" + messageNum + "の投稿のいいねボタンが押されました")
		loginUser := getLoginUser(c)

		messageID, err := strconv.ParseInt(messageNum, 10, 0)
		if err != nil {
			log.Fatal(err)
			return c.NoContent(403)
		}

		judge, err := strconv.ParseBool(likejudge)
		if err != nil {
			log.Fatal(err)
			return c.NoContent(403)
		}
		//いいねされたのか取り消したのかを判定
		if judge == true {
			database.InsertLike(int(messageID), loginUser.ID)
			log.Println("いいね+1")
		} else if judge == false {
			database.DeleteLike(int(messageID), loginUser.ID)
			log.Println("いいね-1")
		}
		//いいねの更新はされるが画面に変更を及ぼさない
		return c.NoContent(204)
	}
}
