package container

//つぶやき情報の構造体
type UserMessage struct {
	ID          int    `JSON:"id"`           //messages
	UserID      int    `JSON:"user_id"`      //messages
	Text        string `JSON:"text"`         //messages
	Account     string `JSON:"account"`      //users
	Name        string `JSON:"name"`         //users
	CreatedDate string `JSON:"created_date"` //messages
	LikeCount   int    `JSON:"like"`         //likes/count
	LikeJudge   bool   `JSON:"loginlike"`    //likes/judge
}
