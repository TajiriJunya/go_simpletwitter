package container

//ユーザ情報の構造体
type User struct {
	ID          int    `JSON:"id"`
	Account     string `JSON:"account"`
	Name        string `JSON:"name"`
	Password    string `JSON:"password"`
	CreatedDate string `JSON:"created_date"`
	UpdateDate  string `JSON:"updated_date"`
}
