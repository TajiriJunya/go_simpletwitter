package database

import (
	"go_simpleTwitter/container"
	"log"
	"strconv"
)

//投稿に対するいいねを取得
func like(ID int) []container.Like {

	messageID := strconv.Itoa(ID)
	db := dbopen()
	sql := "SELECT * FROM likes WHERE message_id = " + messageID + ";"
	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	defer db.Close()

	var messageLikes []container.Like

	for rows.Next() {
		var (
			message_id int
			user_id    int
		)
		if err := rows.Scan(&message_id, &user_id); err != nil {
			log.Fatal(err)
		}
		messageLikes = append(messageLikes, container.Like{MessageID: message_id, UserID: user_id})
	}

	return messageLikes
}

//いいねの追加
func InsertLike(messgeID int, userID int) {

	db := dbopen()
	_, err := db.Exec("INSERT INTO likes (message_id , user_id) VALUES (?,?)", messgeID, userID)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
}

//いいねの取り消し
func DeleteLike(messageID int, userID int) {

	db := dbopen()
	_, err := db.Exec("DELETE FROM likes WHERE message_id=? AND user_id=? ", messageID, userID)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
}
