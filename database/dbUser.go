package database

import (
	"fmt"
	"go_simpleTwitter/container"
	"log"
	"regexp"
)

//アカウントとパスワードが一致するユーザの呼び出し
func SelectLogin(putAccount string, Password string) container.User {
	db := dbopen()
	putPassword := encryption(Password)
	sql := "SELECT id,account,name,password FROM users WHERE account = " + "\"" + putAccount + "\"" + " AND password = " + "\"" + putPassword + "\"" + ";"
	fmt.Println(sql)
	rows, err := db.Query(sql)
	defer rows.Close()
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
	var user container.User
	for rows.Next() {
		var (
			id       int
			account  string
			name     string
			password string
		)
		if err := rows.Scan(&id, &account, &name, &password); err != nil {
			log.Fatal(err)
		}
		user.ID = id
		user.Account = account
		user.Name = name
		user.Password = password

	}
	return user
}

//ログイン中のユーザ情報を取得
func SelectLoginUser(putID string) container.User {
	db := dbopen()
	var user container.User
	rows, err := db.Query("SELECT id,account,name,password FROM users WHERE id = " + putID + ";")
	defer rows.Close()
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var (
			id       int
			account  string
			name     string
			password string
		)
		if err := rows.Scan(&id, &account, &name, &password); err != nil {
			log.Fatal(err)
		}
		user.ID = id
		user.Account = account
		user.Name = name
		user.Password = password

	}
	return user
}

//新規ユーザー登録
func InsertUser(user container.User) bool {
	Account := user.Account
	Name := user.Name
	Password := encryption(user.Password)
	db := dbopen()
	_, err := db.Exec("INSERT INTO users (account, name, password, created_date, updated_date ) VALUES (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)", Account, Name, Password)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

//ユーザ情報の更新
func UpdataUser(user container.User, id int) bool {
	ID := id
	Account := user.Account
	Name := user.Name
	Password := user.Password
	whitespase := regexp.MustCompile(`\s+`)
	db := dbopen()
	defer db.Close()
	//パスワードが入力されなかった場合はアカウント名とユーザ名のみを更新
	if whitespase.MatchString(Password) || Password == "" {
		_, err := db.Exec("UPDATE users SET account = ?, name = ?, updated_date =CURRENT_TIMESTAMP WHERE id = ?", Account, Name, ID)
		if err != nil {
			log.Fatal(err)
			return false
		}
	} else {
		Password = encryption(user.Password)
		_, err := db.Exec("UPDATE users SET account = ?, name = ?, password = ?, updated_date = CURRENT_TIMESTAMP WHERE id = ?", Account, Name, Password, ID)
		if err != nil {
			log.Fatal(err)
			return false
		}
	}
	return true
}

//既に存在するアカウント名がないかを照合
func CheckAccount(putAccount string) bool {
	var account string
	db := dbopen()
	if err := db.QueryRow("SELECT account FROM users WHERE account = ?", putAccount).Scan(&account); err != nil {
		return false
	}
	defer db.Close()

	return true
}

//ユーザアカウントの消去
func DeleteUser(loginUserID int) bool {
	db := dbopen()
	_, err := db.Exec("DELETE FROM users WHERE id=? ", loginUserID)
	defer db.Close()
	if err != nil {
		fmt.Println("DBエラー")
		log.Fatal(err)
		return false
	}
	return true
}
