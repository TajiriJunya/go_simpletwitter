package database

import (
	"log"
)

//新規投稿をDBに挿入
func InsertMessage(userID int, putText string) bool {
	db := dbopen()
	_, err := db.Exec("INSERT INTO messages (user_id, text, created_date, updated_date ) VALUES (?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)", userID, putText)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

//投稿を削除
func DeleteMessage(messageID int) bool {
	db := dbopen()
	_, err := db.Exec("DELETE FROM messages WHERE id=? ", messageID)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}
