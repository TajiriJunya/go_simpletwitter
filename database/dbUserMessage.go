package database

import (
	"go_simpleTwitter/container"
	"log"
	"strconv"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

//画面情報の取得（投稿の全件取得）
func Gettop(c echo.Context) []container.UserMessage {

	db := dbopen()
	rows, err := db.Query("SELECT messages.id,account,name,user_id,text,messages.created_date FROM messages INNER JOIN users ON messages.user_id = users.id ORDER BY created_date DESC limit 1000;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	defer db.Close()

	var userMessages []container.UserMessage

	for rows.Next() {
		var (
			id           int
			account      string
			name         string
			user_id      int
			text         string
			created_date string
		)
		if err := rows.Scan(&id, &account, &name, &user_id, &text, &created_date); err != nil {
			log.Fatal(err)
		}
		likedate := like(id)
		likecount := len(likedate)
		likejudge := likejudge(likedate, c)
		userMessages = append(userMessages, container.UserMessage{ID: id, Account: account, Name: name, UserID: user_id, Text: text, CreatedDate: created_date, LikeCount: likecount, LikeJudge: likejudge})
	}
	return userMessages
}

//ログイン中のユーザが投稿に対していいねをしているかどうかを照合
func likejudge(likedate []container.Like, c echo.Context) bool {

	sess, _ := session.Get("session", c)
	loginUserID, _ := sess.Values["loginUser"].(int)
	loginUser := SelectLoginUser(strconv.Itoa(loginUserID))

	for _, data := range likedate {
		if data.UserID == loginUser.ID {
			return true
		}
	}
	return false
}
