package database

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

//DBへのアクセス
func dbopen() *sql.DB {
	db, err := sql.Open("mysql", "root:root@/go_simple_twitter")
	if err != nil {
		log.Fatal(err)
	}
	return db
}
