package main

import (
	"go_simpleTwitter/handler"
	"html/template"
	"io"
	"log"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

// Template はHTMLテンプレートを利用するためのRenderer Interfaceです。
type Template struct {
	templates *template.Template
}

// Render はHTMLテンプレートにデータを埋め込んだ結果をWriterに書き込みます。
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	//テンプレートとして使用するファイルのパス指定
	t := &Template{
		templates: template.Must(template.ParseGlob("views/*.html")),
	}
	// Echoのインスタンス生成
	e := echo.New()
	log.Println("サーバ起動")
	//HTMLテンプレート情報の代入
	e.Renderer = t
	//CSSファイルの紐づけ
	e.Static("/views/css", "views/css/")
	e.Static("/views/svg", "views/svg/")
	e.Static("/views/js", "views/js/")
	e.Static("/views/ico", "views/ico/")
	//セッションを設定
	e.Use(session.Middleware(sessions.NewCookieStore([]byte("secret"))))
	//ハンドラー処理
	initRouting(e)

	e.Logger.Fatal(e.Start(":1313"))
}

//GETやPOSTの指定
func initRouting(e *echo.Echo) {
	//Get...は基本的に画面表示処理の呼び出し
	e.GET("/top", handler.GetTop())         //topHandler/GetTop
	e.GET("/login", handler.GetLogin())     //loginHandler/GetLogin
	e.GET("/logout", handler.Logout())      //logoutHandler/Logout
	e.GET("/signin", handler.GetSignin())   //signinHandler/GetSgnin
	e.GET("/setting", handler.GetSetting()) //settingHandler/Getsetting

	e.POST("/top", handler.NewPost())                 //topHandler/NewPost
	e.POST("/login", handler.Login())                 //loginHandler/Login
	e.POST("/signin", handler.Signin())               //signinHandler/Signin
	e.POST("/setting", handler.UserEdit())            //settingHandler/UserEdit
	e.POST("/deletemMssage", handler.DeleteMessage()) //deleteHandler/Deletemessage
	e.POST("/deleteUser", handler.DeleteUser())       //deleteHandler/DeleteUser
	e.POST("/like", handler.LikeUpdate())             //likeHandler/LikeUpdate
}
